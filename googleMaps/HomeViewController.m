//
//  HomeViewController.m
//  googleMaps
//
//  Created by Click Labs130 on 10/14/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import "HomeViewController.h"
#import<GoogleMaps/GoogleMaps.h>
@interface HomeViewController ()
{
    CLLocationManager *manager;
}
@end

@implementation HomeViewController
GMSMapView *googleMap;
MKMapView *appleMap;
UITextField *longitude;
UITextField *latitude;
UIButton *button;
CLLocation *currentLocation;
GMSMarker *marker;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    googleMap = [GMSMapView mapWithFrame:CGRectMake(10, 0, 300, 200) camera:nil];
    googleMap.myLocationEnabled = YES;
    
    googleMap.delegate = self;
    [self.view addSubview:googleMap];
    
    appleMap = [[MKMapView alloc]initWithFrame:CGRectMake(10, 205, 300, 200)];
    //appleMap.userInteractionEnabled=YES;
    [self.view addSubview:appleMap];
    

    manager = [[CLLocationManager alloc]init];
    manager.delegate = self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
	// Do any additional setup after loading the view, typically from a nib.
    
    latitude=[[UITextField alloc]init];
    latitude.frame=CGRectMake(10, 415, 80, 20);
    latitude.backgroundColor=[UIColor lightGrayColor];
    latitude.placeholder=@"latitude";
    [self.view addSubview:latitude];
    
    longitude=[[UITextField alloc]init];
    longitude.frame=CGRectMake(10, 440, 80, 20);
    longitude.backgroundColor=[UIColor lightGrayColor];
    longitude.placeholder=@"longitude";
    [self.view addSubview:longitude];
    
    
    button=[[UIButton alloc]init];
    button.frame=CGRectMake(140, 427, 100, 50);
    button.backgroundColor=[UIColor redColor];
    [button addTarget:self action:@selector(getlocation:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    
    
   }
//- (void)getLocation{
//    [manager startUpdatingLocation];
//    
//}
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    NSLog(@"didFailWithError: %@", error);
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//}
-(void)getlocation:(id)sender {
    [manager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSLog(@"didUpdateToLocation: %@", newLocation);
  currentLocation = newLocation;
   
   if (currentLocation != nil) {
       longitude.text = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.longitude];
        latitude.text = [NSString stringWithFormat:@"%.2f", currentLocation.coordinate.latitude];
   
   
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
    longitude:currentLocation.coordinate.longitude
                                                                 zoom:2];
//        googleMap.camera = camera;
    marker = [[GMSMarker alloc] init];
    marker.position = currentLocation.coordinate;
       
    marker.map = googleMap;
        googleMap.delegate = self;
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
       
       [self address];
    }
}

-(void) address{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
                                                        longitude:currentLocation.coordinate.longitude];
    
    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       
                       if (placemarks && placemarks.count > 0)
                       {
                           CLPlacemark *placemark = placemarks[0];
                           
                           NSDictionary *addressDictionary =
                           placemark.addressDictionary;
                           
                           NSLog(@"%@", addressDictionary);
                           NSArray *address= addressDictionary[@"FormattedAddressLines"];
                           NSString *title = [address componentsJoinedByString: @","];
                           //
                           
                           
                           marker.title = [NSString stringWithFormat:@"%@",title];
                           
                           //point.coordinate = currentLocation.coordinate;
                         //  point.title = [NSString stringWithFormat:@"%@",title];
                          // [appleMap addAnnotation:point];
                           [manager stopUpdatingLocation];
                       }
                   }];

}

- (void)mapView:(GMSMapView *)mapView
didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = coordinate;
    
      marker.map = googleMap;
          // googleMap.delegate = self;
    [self address];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
