//
//  HomeViewController.h
//  googleMaps
//
//  Created by Click Labs130 on 10/14/15.
//  Copyright (c) 2015 Click Labs130. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>


@interface HomeViewController : UIViewController<CLLocationManagerDelegate,GMSMapViewDelegate,MKMapViewDelegate>

@end
